import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:sanad/screens/auth_screen/login.dart';
import 'package:sanad/simple_bloc_delegate.dart';


void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primaryColor: Colors.teal,
            primarySwatch: Colors.teal,
            buttonColor: Colors.teal,
          ),
          home: LoginPage(),
    );
  }
}

