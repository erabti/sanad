import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CommunityPage extends StatefulWidget {
  @override
  _CommunityPageState createState() => _CommunityPageState();
}

class _CommunityPageState extends State<CommunityPage> {
   List<Widget> posts;
   TextEditingController controller = TextEditingController();
   @override
  void initState() {
    posts = [
      TextField(
        textDirection: TextDirection.rtl,
        controller: controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            suffixIcon: IconButton(
              icon: Icon(Icons.send),
              onPressed: (){
                setState(() {
                  posts.insert(1, getCard(
                    "أحمد الرابطي",
                    "متقدم",
                    "assets/profile.jpeg",
                    controller.text,
                    0,
                    0,
                    0
                  ));
                });
              },
            ),
            hintText: "اكتـب منشورك هنا..."),
      ),
      getCard(
          "محمد المزوغي",
          "مبتدئ",
          "assets/image1.png",
          """للتوضيح انا عندي اي فون ٦
ارغب ان ارى اولادي على شاشه التلفزيون اثناء مكالمات الفيديو ممكن
وماهي البرامح المناسبه المجانيه""",
          5),
      getCard(
          "على جمعة",
          "صاحب فكرة",
          "assets/image2.jpg",
          "منتبيس ثصهخقاب منشسىبنمت شسنتبيﻻخهص ثتثبىشس نتيتب نمشستيبه صشثخبتنمشستبشسنيبه شس"
              "مشسنتيبمنشستيبمنتشسيمبنتشسمينبتمشنسيتبمنشسيتبنمتشسيمبشمنسيتبنمشسيتبنمشسيتبنمشستيبن"
              "شسمينبتشمنسيبتمنشسيتبمنشسيتبمنشستبمنتشسهخبتثصششسمنبي تشسيبت شهمسيتب منشسيبى شسمينبت ",
          2),
      getCard(
          "محمد المزوغي",
          "مبتدئ",
          "assets/image1.png",
          "منتبيس ثصهخقاب منشسىبنمت شسنتبيﻻخهص ثتثبىشس نتيتب نمشستيبه صشثخبتنمشستبشسنيبه شس"
              "مشسنتيبمنشستيبمنتشسيمبنتشسمينبتمشنسيتبمنشسيتبنمتشسيمبشمنسيتبنمشسيتبنمشسيتبنمشستيبن"
              "شسمينبتشمنسيبتمنشسيتبمنشسيتبمنشستبمنتشسهخبتثصششسمنبي تشسيبت شهمسيتب منشسيبى شسمينبت ",
          5),
      getCard(
          "محمد المزوغي",
          "مبتدئ",
          "assets/image1.png",
          "منتبيس ثصهخقاب منشسىبنمت شسنتبيﻻخهص ثتثبىشس نتيتب نمشستيبه صشثخبتنمشستبشسنيبه شس"
              "مشسنتيبمنشستيبمنتشسيمبنتشسمينبتمشنسيتبمنشسيتبنمتشسيمبشمنسيتبنمشسيتبنمشسيتبنمشستيبن"
              "شسمينبتشمنسيبتمنشسيتبمنشسيتبمنشستبمنتشسهخبتثصششسمنبي تشسيبت شهمسيتب منشسيبى شسمينبت ",
          5)
    ];
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("المنتدى", style: TextStyle(fontWeight: FontWeight.bold)),
        actions: [
          Padding(
            padding: EdgeInsets.only(left: 3),
            child: Icon(Icons.filter_list),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Card(
              child: Column(
                children: posts,
              ),
            ),
          ],
        ),
      ),
    );
  }

   Widget getCard(
       String name, String label, String image, String postText, int initLike, [int comment, int share]) {
     int likes = initLike ?? 3;
     int comments = comment ?? likes -1;
     int shares = share ?? likes + 2;
     return Card(
       child: Column(
         children: <Widget>[
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Text(
                 name,
                 style: TextStyle(fontWeight: FontWeight.bold),
               ),
               Column(
                 children: <Widget>[
                   CircleAvatar(
                     backgroundImage: AssetImage(image),
                     radius: 20,
                   ),
                   Text(label)
                 ],
               )
             ],
           ),
           Divider(),
           Text(postText, textAlign: TextAlign.start, textDirection: TextDirection.rtl),
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: <Widget>[
               Row(
                 children: <Widget>[
                   Column(
                     children: <Widget>[
                       IconButton(
                         icon: Icon(FontAwesomeIcons.thumbsUp),
                         onPressed: () {
                           setState(() {
                             likes += 1;
                           });
                         },
                       ),
                       Text((likes).toString())
                     ],
                   ),
                   Column(
                     children: <Widget>[
                       IconButton(
                         icon: Icon(FontAwesomeIcons.comments),
                         onPressed: () {
                           setState(() {
                             likes += 1;
                           });
                         },
                       ),
                       Text((comments).toString())
                     ],
                   ),
                 ],
               ),
               Column(
                 children: <Widget>[
                   IconButton(
                     icon: Icon(FontAwesomeIcons.share),
                     onPressed: () {
                       setState(() {
                         likes += 1;
                       });
                     },
                   ),
                   Text((shares).toString())
                 ],
               ),
             ],
           )
         ],
       ),
     );
   }
}
