import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:gradient_widgets/gradient_widgets.dart';

class HomePage extends StatelessWidget {
  Column buildColumn(String title, String i1, i2, i3, i4, t1, t2, t3, t4) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(4),
          child: Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      width: 130,
                      height: 80,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(i1),
                      )),
                    ),
                  ),
                  Text(t1)
                ],
              ),
              Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      width: 130,
                      height: 80,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(i2),
                      )),
                    ),
                  ),
                  Text(t2)
                ],
              ),
              Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      width: 130,
                      height: 80,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(i3),
                      )),
                    ),
                  ),
                  Text(t3)
                ],
              ),
              Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      width: 130,
                      height: 80,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(i4),
                      )),
                    ),
                  ),
                  Text(t4)
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  HomePage();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(gradient: Gradients.rainbowBlue),
            width: double.maxFinite,
            padding: EdgeInsets.only(top: 50, bottom: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    Icon(
                      Icons.notifications_active,
                      color: Colors.white,
                      size: 27,
                    ),
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.red.shade700, shape: BoxShape.circle),
                      child: Text("2",
                          style: TextStyle(color: Colors.white, fontSize: 6)),
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        CircularPercentIndicator(
                          radius: 70,
                          percent: 0.76,
                        ),
                        CircleAvatar(
                          radius: 30,
                          backgroundImage: AssetImage("assets/profile.jpeg"),
                        )
                      ],
                    ),
                    Text(
                      "رائد اعمال",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                Icon(Icons.apps, color: Colors.white),
              ],
            ),
          ),
          Container(
              alignment: Alignment.centerRight,
              child: buildColumn(
                "احدث الدورات التدريبية",
                "assets/pic1.jpeg",
                "assets/pic2.jpeg",
                "assets/pic3.jpeg",
                "assets/pic4.jpeg",
                "نبذة عن ريادة الاعمال",
                "الفكرة",
                "سوق العمل",
                "التسويق",
              )),
          Container(
              alignment: Alignment.centerRight,
              child: buildColumn(
                "اخر الاحداث",
                "assets/p1.jpg",
                "assets/p2.jpg",
                "assets/p3.jpeg",
                "assets/pic3.jpeg",
                "هاكاثون ستارت اب",
                "ساعة برمجة",
                "مسابقة ريادة الاعمال",
                "اليوم العالمي لريادة الاعمال",
              )),
          Container(
              alignment: Alignment.centerRight,
              child: buildColumn(
                "اخر الاخبار",
                "assets/p3.jpeg",
                "assets/p1.jpg",
                "assets/pic3.jpeg",
                "assets/pic4.jpeg",
                "نبذة عن ريادة الاعمال",
                "اخبار الاسهم العالمية",
                "اخبار سوق العمل",
                "اخبار التسويق",
              )),
        ],
      ),
    );
  }
}
