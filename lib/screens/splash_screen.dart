import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/logo.png",width: 130,height: 130,),
            Column(
              children: <Widget>[
                CircularProgressIndicator(),
                Padding(
                  padding: EdgeInsets.only(bottom: 15),
                ),
                Text("جاري التحميل...",style: TextStyle(fontWeight: FontWeight.bold),textDirection: TextDirection.rtl,)
              ],
            )
          ],
        ),
      ),
    );
  }
}
