import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:introduction_screen/model/page_view_model.dart';

class WelcomePage extends StatelessWidget {
  final connectionPage = PageViewModel(
    "",
    "تواصل مع مجتمع رواد الاعمال في ليبيا",
    Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Image.asset("assets/connection.png")),
  );
  final skillsPage = PageViewModel(
          "\n\n",
      "استلهم الافكار الابداعية من خبراء ريادة الأعمال",
      Container(
        margin: EdgeInsets.symmetric(vertical:60),
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset("assets/skills.png"))),
    bodyTextStyle: TextStyle(fontSize: 21),
  );
  final ideasPage = PageViewModel(
      "",
      "تعلم المهارات الاساسية لرواد الاعمال",
      Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Image.asset("assets/ideas.png")));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IntroductionScreen(
        pages: [
          skillsPage,
          connectionPage,
          ideasPage,
        ],
        showSkipButton: true,
        done: Text("انهاء",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        skip: Text("تخطي", style: TextStyle(fontWeight: FontWeight.bold)),
        onDone: () {

        },
      ),
    );
  }
}
