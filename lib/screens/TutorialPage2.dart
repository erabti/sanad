import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sanad/screens/TutorialPage.dart';
import 'package:open_file/open_file.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sanad/screens/TutorialPage3.dart';

class TutorialPage2 extends StatefulWidget {
  @override
  _TutorialPageState2 createState() => _TutorialPageState2();
}

class _TutorialPageState2 extends State<TutorialPage2> {
  Widget progressCircle(String num, bool active, [bool test = false]) =>
      Container(
        width: 23,
        height: 23,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border:
              Border.all(color: active ? Colors.blue.shade800 : Colors.blue),
          color: active ? Colors.blue : Colors.white,
        ),
        child: Center(
            child: test
                ? Icon(
                    Icons.check,
                    size: 18,
                    color: active ? Colors.white : Colors.blue,
                  )
                : Text(
                    num,
                    style: TextStyle(
                        color: active ? Colors.white : Colors.blue,
                        fontWeight: FontWeight.bold),
                  )),
      );

  Widget get progress => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          progressCircle("1", false),
          Container(height: 1, width: 20, color: Colors.black),
          progressCircle("2", true),
          Container(height: 1, width: 20, color: Colors.black),
          progressCircle("3", false),
          Container(height: 1, width: 20, color: Colors.black),
          progressCircle("", false, true),
        ],
      );

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => TutorialPage()))),
          title: Text("اساسيات ريادة الاعمال"),
          actions: <Widget>[
            IconButton(
              icon:
                  Text("التالي", style: TextStyle(fontWeight: FontWeight.bold)),
              onPressed: () => Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => TutorialPage3())),
            )
          ],
        ),
        body: Container(
          margin: EdgeInsets.only(top: 4),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                progress,
                Divider(),
                Card(
                  margin: EdgeInsets.only(top: 10),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Text(
                              "ريادة الاعمال الاجتماعية استدامة فعالة للتنمية",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 19),
                              textAlign: TextAlign.start,
                            ),
                            Text(
                              "تهدف التنمية الاجتماعية الى تشجيع أبناء المجتمع على التغيير والإنتقال إلى وضع أكثر تطوراً ونشر القيم والعادات والتقاليد السامية وأهمها التعاون والصدق والالتزام بأداء المهام وانجاز الأعمال ومحاربة المشكلات التي تركها الوضع الإقتصادي المتدهور كالبطالة والهجرة الداخلية، وتحسين الوضع الاجتماعي لأبناء المجتمع ورفع المستوى المعيشي بريادة الأعمال الأجتماعية من خلال تنظيم وإدارة تحديات اجتماعية تحقق تغييراً اجتماعياً مطلوباً يقاس بالربح المادي و بالقيمة الاجتماعية باستخدام الأساليب الإبداعية والمبتكرة لتنمية المشروعات والمؤسسات التى تحقق تأثير اجتماعى واسع النطاق، وتضمن تعزيزاً جلياً للرخاء الاقتصادي المشترك والقضاء التدريجي على الفقر المدقع وهذا حلاً سحرياً ومشجعاً للسلوك الايجابي ا"
                                  "لدافع لعملية التنمية الاجتماعية ومحتوياً للمشكلات الاجتماعية.",
                              textAlign: TextAlign.start,
                            ),
                            Text(
                              "قياس آثار الريادة الاجتماعية",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 19,
                                  color: Colors.blue.shade900),
                              textAlign: TextAlign.start,
                            ),
                            Image.asset("assets/t1.jpg"),
                            Divider(),
                            Text(
                                "يمكن قياس أهم آثار الريادة الاجتماعية على التنمية في المجتمع وبشكل مستدام وفق المستويات التالية:"),
                            Padding(
                              padding: EdgeInsets.only(right: 8),
                              child: Text(
                                  "1- المدى القصير: تغييرات ملموسة في اقتصاد المجتمع (خلق فرص عمل، توليد الناتج، أو زيادة الادخار )."),
                            ),
                            Padding(
                              padding: EdgeInsets.only(right: 8),
                              child: Text(
                                  "2- المدى المتوسط: تتجلى قيمة الريادة الاجتماعية في كونها نموذجاً محتملًا يعمل على رفاهية المجتمع وتحسين أوضاعه، ومن ثم يقاس نجاح الريادة الاجتماعية بقدرتها على زيادة الإنتاجية،والمشروعات التنموية."),
                            ),
                            Text(
                                "إن قيمة وفعالية رأس المال الاجتماعي ينصب في العلاقات الاجتماعية ودور التعاون والثقة في تحقيق الأهداف الاقتصادية ،ويتألف من شبكات اجتماعية وشبكات مشاركة مدنية، وعادات مشتركة لها تأثير على إنتاجية المجتمع وله قيمة تؤثر على إنتاجية الفرد أو المجموعة ، ومن ثم تظهر الإمكانية في تحويل رأس المال الاجتماعي إلي رأس مال مادي ويعد معياراً لنجاح الديمقراطية والمشاركة السياسية."),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            InkWell(
                              onTap: () async {
                                await OpenFile.open("/storage/emulated/0/Download/business_model_canvas_poster.pdf");
                              },
                              child: Card(
                                child: Column(
                                  children: <Widget>[
                                    Icon(
                                      FontAwesomeIcons.filePdf,
                                      size: 40,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 2.5),
                                      child: Text("ملحق"),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Divider(),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text("تعليقات:",
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.bold)),
                ),
                TextField(
                  textDirection: TextDirection.rtl,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.send),
                        onPressed: () {},
                      ),
                      hintText: "اكتـب تعليقك عنا هنا..."),
                ),
                getCard(
                    "محمد المزوغي",
                    "مبتدئ",
                    "assets/image1.png",
                    "منتبيس ثصهخقاب منشسىبنمت شسنتبيﻻخهص ثتثبىشس نتيتب نمشستيبه صشثخبتنمشستبشسنيبه شس"
                    "مشسنتيبمنشستيبمنتشسيمبنتشسمينبتمشنسيتبمنشسيتبنمتشسيمبشمنسيتبنمشسيتبنمشسيتبنمشستيبن"
                    "شسمينبتشمنسيبتمنشسيتبمنشسيتبمنشستبمنتشسهخبتثصششسمنبي تشسيبت شهمسيتب منشسيبى شسمينبت ",
                    5),
                getCard(
                    "محمد المزوغي",
                    "مبتدئ",
                    "assets/image1.png",
                    "منتبيس ثصهخقاب منشسىبنمت شسنتبيﻻخهص ثتثبىشس نتيتب نمشستيبه صشثخبتنمشستبشسنيبه شس"
                    "مشسنتيبمنشستيبمنتشسيمبنتشسمينبتمشنسيتبمنشسيتبنمتشسيمبشمنسيتبنمشسيتبنمشسيتبنمشستيبن"
                    "شسمينبتشمنسيبتمنشسيتبمنشسيتبمنشستبمنتشسهخبتثصششسمنبي تشسيبت شهمسيتب منشسيبى شسمينبت ",
                    5)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getCard(
      String name, String label, String image, String postText, int initLike,
      [int comment, int share]) {
    return Card(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                name,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text("منذ 5 د",
                  style: TextStyle(fontStyle: FontStyle.italic, fontSize: 12)),
              Column(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: AssetImage(image),
                    radius: 20,
                  ),
                  Text(label)
                ],
              )
            ],
          ),
          Divider(),
          Text(postText,
              textAlign: TextAlign.start, textDirection: TextDirection.rtl),
        ],
      ),
    );
  }
}
