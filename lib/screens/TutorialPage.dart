import 'package:flutter/material.dart';
import 'package:sanad/screens/TutorialPage2.dart';
import 'package:youtube_player/youtube_player.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TutorialPage extends StatefulWidget {
  @override
  _TutorialPageState createState() => _TutorialPageState();
}

class _TutorialPageState extends State<TutorialPage> {
  Widget progressCircle(String num, bool active, [bool test = false]) =>
      Container(
        width: 23,
        height: 23,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          border:
              Border.all(color: active ? Colors.blue.shade800 : Colors.blue),
          color: active ? Colors.blue : Colors.white,
        ),
        child: Center(
            child: test
                ? Icon(
                    Icons.check,
                    size: 18,
                    color: active ? Colors.white : Colors.blue,
                  )
                : Text(
                    num,
                    style: TextStyle(
                        color: active ? Colors.white : Colors.blue,
                        fontWeight: FontWeight.bold),
                  )),
      );

  Widget get progress => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          progressCircle("1", true),
          Container(height: 1, width: 20, color: Colors.black),
          progressCircle("2", false),
          Container(height: 1, width: 20, color: Colors.black),
          progressCircle("3", false),
          Container(height: 1, width: 20, color: Colors.black),
          progressCircle("", false, true),
        ],
      );
  VideoPlayerController _videoController;

  @override
  void dispose() {
    _videoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: Text("اساسيات ريادة الاعمال"),
          actions: <Widget>[
            IconButton(
              icon:
                  Text("التالي", style: TextStyle(fontWeight: FontWeight.bold)),
              onPressed: () {
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => TutorialPage2()));
              },
            )
          ],
        ),
        body: Container(
          margin: EdgeInsets.only(top: 4),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                progress,
                DropdownButton(
                  isExpanded: true,
                  onChanged: (x) {
                    if (_videoController == null) return;
                    switch (x) {
                      case "idea":
                        _videoController.seekTo(Duration(minutes: 2));
                        break;
                      case "market":
                        _videoController.seekTo(Duration(minutes: 6));
                        break;
                      case "trademark":
                        _videoController.seekTo(Duration(minutes: 10));
                        break;
                      case "project":
                        _videoController.seekTo(Duration(minutes: 15));
                        break;
                    }
                  },
                  hint: Padding(
                    padding: EdgeInsets.only(right: 4),
                    child: Text(
                      "المحتويات",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  value: null,
                  items: [
                    DropdownMenuItem(
                      child: Text("كتابة الفكرة"),
                      value: "idea",
                    ),
                    DropdownMenuItem(
                      child: Text("دراسة السوق"),
                      value: "market",
                    ),
                    DropdownMenuItem(
                      child: Text("بناء العلامة التجارية"),
                      value: "trademark",
                    ),
                    DropdownMenuItem(
                        child: Text("اطلاق المشروع"), value: "project"),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(top: 5),
                  child: YoutubePlayer(
                    context: context,
                    quality: YoutubeQuality.FHD,
                    source: "EFdsDVEJ2ao",
                    keepScreenOn: true,
                    callbackController: (controller) {
                      _videoController = controller;
                    },
                  ),
                ),
                Card(
                  margin: EdgeInsets.only(top: 10),
                  child: Text(
                      "في هذا الدرس سنقوم بتعلم اساسيات ريادة الاعمال بإخر متوصل اليه العالم في هذا المجال الرائع"),
                ),
                Divider(),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text("تعليقات:",
                      style:
                          TextStyle(fontSize: 21, fontWeight: FontWeight.bold)),
                ),
                TextField(
                  textDirection: TextDirection.rtl,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.send),
                        onPressed: () {},
                      ),
                      hintText: "اكتـب تعليقك عنا هنا..."),
                ),
                getCard(
                    "محمد المزوغي",
                    "مبتدئ",
                    "assets/image1.png",
                    "لدي ملاحظة صغيرة يجب توضيح النقاط المهمة قبل التعمق في محتوى الدرس. شكرا ",
                    5),
                getCard(
                    "محمد عبد السلام",
                    "متقدم",
                    "assets/image1.png",
                    "استمتعت كثيرا, شكراا",
                    5)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getCard(
      String name, String label, String image, String postText, int initLike,
      [int comment, int share]) {
    return Card(

      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  name,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text("منذ 5 د",
                    style: TextStyle(fontStyle: FontStyle.italic, fontSize: 12)),
                Column(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage(image),
                      radius: 20,
                    ),
                    Text(label)
                  ],
                )
              ],
            ),
            Divider(),
            Text(postText,
                textAlign: TextAlign.start, textDirection: TextDirection.rtl),
          ],
        ),
      ),
    );
  }
}
