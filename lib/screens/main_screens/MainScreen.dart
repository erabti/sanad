import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sanad/screens/HomePage.dart';
import 'package:sanad/screens/community_page.dart';
import 'package:sanad/screens/events_page.dart';
import 'package:sanad/screens/new_page.dart';
import 'package:sanad/screens/training_page.dart';
import 'package:flutter_inner_drawer/inner_drawer.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int index = 0;

  List<Widget> pages = [
    HomePage(),
    TrainingPage(),
    CommunityPage(),
    NewsPage(),
    EventsPage()
  ];

  @override
  Widget build(BuildContext context) {
    return InnerDrawer(
      position: InnerDrawerPosition.end,
      animationType: InnerDrawerAnimation.quadratic,
      child: Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text("الاسئلة الشائعة"),
              )
            ],
          ),
        ),
      ),
      scaffold: Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          bottomNavigationBar: Theme(
            data: ThemeData(iconTheme: IconThemeData(color: Colors.blue)),
            child: BottomNavigationBar(
              currentIndex: index,
              fixedColor: Colors.black,
              onTap: (i) => setState(() => index = i),
              items: [
                BottomNavigationBarItem(
                    title: Text(
                      'الرئيسية',
                      style: TextStyle(color: Colors.black),
                    ),
                    icon: Icon(
                      FontAwesomeIcons.home,
                      color: Colors.blue,
                    )),
                BottomNavigationBarItem(
                    title: Text("تدريب", style: TextStyle(color: Colors.black)),
                    icon: Icon(
                      FontAwesomeIcons.bookReader,
                      color: Colors.blue,
                    )),
                BottomNavigationBarItem(
                    title: Text("منتدى", style: TextStyle(color: Colors.black)),
                    icon: Icon(
                      FontAwesomeIcons.userFriends,
                      color: Colors.blue,
                    )),
                BottomNavigationBarItem(
                    title: Text("اخبار", style: TextStyle(color: Colors.black)),
                    icon: Icon(
                      FontAwesomeIcons.newspaper,
                      color: Colors.blue,
                    )),
                BottomNavigationBarItem(
                    title: Text("احداث", style: TextStyle(color: Colors.black)),
                    icon: Icon(
                      FontAwesomeIcons.calendar,
                      color: Colors.blue,
                    ))
              ],
            ),
          ),
          body: Container(
            child: pages[index],
          ),
        ),
      ),
    );
  }
}
