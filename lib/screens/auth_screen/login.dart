import 'package:flutter/material.dart';
import 'package:sanad/screens/auth_screen/register.dart';
import 'package:sanad/screens/main_screens/MainScreen.dart';
import 'package:sanad/widgets/button.dart';
import 'package:sanad/widgets/custom_icon_button.dart';
import 'package:sanad/widgets/field.dart';
import 'package:sanad/widgets/logo.dart';
import 'package:sanad/widgets/page.dart';
import 'package:sanad/widgets/separator.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatelessWidget {
  void goto(BuildContext context, Widget widget) => Navigator.of(context)
      .push(MaterialPageRoute(builder: (context) => widget));

  @override
  Widget build(BuildContext context) {
    return Page(
      scrollable: true,
      child: Column(
        children: <Widget>[
          Logo(),
          Field("البريد الاكتروني"),
          Field("كلمة السر"),
          Button(onPressed: () => goto(context, MainScreen()), label: "الدخول"),
          Button(
            onPressed: () => goto(context, RegisterScreen()),
            label: "التسجيل",
            isAccent: true,
          ),
          Separator("أو"),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButtons.facebook(() {}),
              IconButtons.google(() {}),
            ],
          )
        ],
      ),
    );
  }
}

class Utils {
  static Future<bool> isWelcomeShown() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var shown = sharedPreferences.getBool("welcomeShown");
    if (shown == null) {
      sharedPreferences.setBool("welcomeShown", true);
      return false;
    } else {
      sharedPreferences.setBool("welcomeShown", true);
      return true;
    }
  }
}
