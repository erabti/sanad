import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  bool isAgreeChecked = false;

  set profilePicture(Widget x) => profilePicture = x;

  Widget get profilePicture => InkWell(
        onTap: () async {
          File file = await ImagePicker.pickImage(
              source: ImageSource.gallery, maxHeight: 640, maxWidth: 480);
          setState(() {
            profilePicture = Image.memory(file.readAsBytesSync());
          });
        },
        child: Container(
          width: 80,
          height: 80,
          padding: EdgeInsets.only(top: 0),
          decoration: BoxDecoration(
              shape: BoxShape.circle, border: Border.all(color: Colors.blue)),
          child: Icon(
            Icons.camera_enhance,
            size: 40,
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Directionality(
        textDirection: TextDirection.rtl,
        child: Container(
          child: SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Form(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(right: 80, left: 80, top: 40),
                        child: Image.asset("assets/logo.png", height: 120),
                      ),
                      profilePicture,
                      Padding(
                        padding: EdgeInsets.only(bottom: 5),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: "اسم المستخدم",
                            border: OutlineInputBorder(
                                gapPadding: 50, borderSide: BorderSide.none),
                          ),
                        ),
                      ),
                      Container(
                        height: 0.5,
                        width: 280,
                        padding: EdgeInsets.symmetric(vertical: 60),
                        color: Colors.blue,
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: "البريد الالكتروني",
                            border: OutlineInputBorder(
                              gapPadding: 50,
                              borderSide: BorderSide.none,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 0.5,
                        width: 280,
                        padding: EdgeInsets.symmetric(vertical: 60),
                        color: Colors.blue,
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: "كلمة سر",
                            border: OutlineInputBorder(
                                gapPadding: 50, borderSide: BorderSide.none),
                          ),
                        ),
                      ),
                      Container(
                        height: 0.5,
                        width: 280,
                        padding: EdgeInsets.symmetric(vertical: 60),
                        color: Colors.blue,
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 5),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: "تأكيد كلمة السر",
                            border: OutlineInputBorder(
                                gapPadding: 50, borderSide: BorderSide.none),
                          ),
                        ),
                      ),
                      Container(
                        height: 0.5,
                        width: 280,
                        padding: EdgeInsets.symmetric(vertical: 60),
                        color: Colors.blue,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Row(
                          children: <Widget>[
                            Text("الموافقة على الشروط والسياسات"),
                            Checkbox(
                              onChanged: (b) {
                                setState(() => isAgreeChecked = b);
                              },
                              value: isAgreeChecked,
                            )
                          ],
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {},
                        padding:
                            EdgeInsets.symmetric(horizontal: 80, vertical: 3.5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        child: Text("التسجيل",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 21)),
                        color: Color(0xff22b573),
                      ),
                    ],
                  ),
                ),
                Directionality(
                  textDirection: TextDirection.ltr,
                  child: Positioned(
                    top: 30,
                    left: 0,
                    child: IconButton(
                      icon:
                          Icon(Icons.arrow_back, color: Colors.black, size: 30),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
