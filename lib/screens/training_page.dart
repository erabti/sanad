import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:sanad/screens/TutorialPage.dart';

class TrainingPage extends StatefulWidget {
  @override
  _TrainingPageState createState() => _TrainingPageState();
}

class _TrainingPageState extends State<TrainingPage> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    Widget leaningImage = Stack(
      children: <Widget>[        Image.asset("assets/learning.jpeg"),
        Container(
          child: Center(
            child: Text(
              "تعلم اساسيات ريادة الاعمال",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
    Widget creativtyImage = Stack(
      children: <Widget>[
        Image.asset("assets/creativity.jpeg"),
        Container(
          child: Center(
            child: Text(
              "تواصل مع مجتمع رواد الأعمال في ليبيا",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
    Widget planningImage = Stack(
      children: <Widget>[
        Image.asset("assets/planning.jpeg"),
        Container(
          child: Center(
            child: Text(
              "كن على إطلاع بأخر أحداث ريادة الأعمال",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 21,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
    List<Widget> items = [planningImage, creativtyImage, leaningImage];
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "التدريب",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.only(left:3),
            child: Icon(Icons.search),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                CarouselSlider(
                  items: items,
                  autoPlay: true,
                  onPageChanged: (index) => setState(() => _current = index),
                ),
                Positioned(
                    bottom: 2.5,
                    left: 0.0,
                    right: 0.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 8.0,
                          height: 8.0,
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color:
                                  _current == 0 ? Colors.blue : Colors.white),
                        ),
                        Container(
                          width: 8.0,
                          height: 8.0,
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color:
                                  _current == 1 ? Colors.blue : Colors.white),
                        ),
                        Container(
                          width: 8.0,
                          height: 8.0,
                          margin: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 2.0),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color:
                                  _current == 2 ? Colors.blue : Colors.white),
                        )
                      ],
                    ))
              ],
            ),
            Container(
                alignment: Alignment.centerRight,
                child: buildColumn(
                  "اساسيات ريادة الاعمال",
                  "assets/pic1.jpeg",
                  "assets/pic2.jpeg",
                  "assets/pic3.jpeg",
                  "assets/pic4.jpeg",
                  "نبذة عن ريادة الاعمال",
                  "الفكرة",
                  "سوق العمل",
                  "التسويق",
                )),
            Container(
                alignment: Alignment.centerRight,
                child: buildColumn(
                  "اساسيات التسويق",
                  "assets/pic2.jpeg",
                  "assets/pic3.jpeg",
                  "assets/pic1.jpeg",
                  "assets/pic4.jpeg",
                  "نبذة عن التسويق",
                  "الفكرة",
                  "سوق العمل",
                  "التسويق",
                )),
            Container(
                alignment: Alignment.centerRight,
                child: buildColumn(
                  "اساسيات ريادة الاعمال",
                  "assets/pic3.jpeg",
                  "assets/pic4.jpeg",
                  "assets/pic1.jpeg",
                  "assets/pic2.jpeg",
                  "نبذة عن ريادة الاعمال",
                  "الفكرة",
                  "سوق العمل",
                  "التسويق",
                )),
          ],
        ),
      ),
    );
  }

  Column buildColumn(String title, String i1, i2, i3, i4, t1, t2, t3, t4) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(4),
          child: Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              InkWell(
                onTap:(){
                  Navigator.of(context).push(MaterialPageRoute(builder: (context)=>TutorialPage()));
                },
                child: Column(
                  children: <Widget>[
                    Card(
                      child: Container(
                        width: 110,
                        height: 60,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(i1),
                            )),
                      ),
                    ),
                    Text(t1)
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      width: 110,
                      height: 60,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(i2),
                          )),
                    ),
                  ),
                  Text(t2)
                ],
              ),
              Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      width: 110,
                      height: 60,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(i3),
                          )),
                    ),
                  ),
                  Text(t3)
                ],
              ),
              Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      width: 110,
                      height: 60,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(i4),
                          )),
                    ),
                  ),
                  Text(t4)
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}

class TrainingPreview extends StatelessWidget {
  final Image image;
  TrainingPreview(this.image);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: image,
    );
  }
}
