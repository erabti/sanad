import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class NewsPage extends StatefulWidget {
  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  Widget getCard(
      String name, String label, String image, String postText, int initLike,
      [int comment, int share, String i]) {
    int likes = initLike ?? 3;
    int comments = comment ?? likes - 1;
    int shares = share ?? likes + 2;
    return Card(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                name,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Column(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: AssetImage(image),
                    radius: 20,
                  ),
                  Text(label)
                ],
              )
            ],
          ),
          Divider(),
          i == 'a' ? Image.asset("assets/p3.jpeg") : Container(),
          i == 'b' ? Image.asset("assets/p1.jpg") : Container(),
          i == 'c' ? Image.asset("assets/p2.jpg") : Container(),
          i == 'd' ? Image.asset("assets/pic4.jpeg") : Container(),
          Text(
            postText,
            textAlign: TextAlign.start,
            textDirection: TextDirection.rtl,
            maxLines: 4,
            overflow: TextOverflow.ellipsis,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(FontAwesomeIcons.thumbsUp),
                        onPressed: () {
                          setState(() {
                            likes += 1;
                          });
                        },
                      ),
                      Text((likes).toString())
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(FontAwesomeIcons.comments),
                        onPressed: () {
                          setState(() {
                            likes += 1;
                          });
                        },
                      ),
                      Text((comments).toString())
                    ],
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  IconButton(
                    icon: Icon(FontAwesomeIcons.share),
                    onPressed: () {
                      setState(() {
                        likes += 1;
                      });
                    },
                  ),
                  Text((shares).toString())
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("احداث", style: TextStyle(fontWeight: FontWeight.bold)),
          actions: <Widget>[Icon(Icons.filter_list)],
        ),
        body: SingleChildScrollView(
          child: Table(
            children: <TableRow>[
              TableRow(
                children: <Widget>[
                  getCard(
                      "ايفون اسلام",
                      "",
                      "assets/iphoneislam-logo.png",
                      """نواصل معكم وبشكل أسبوعي تقديم اختياراتنا وعروضنا لأفضل التطبيقات، بحسب اختيارات محرري آي-فون إسلام. بحيث تمثل دليلاً كاملاً يوفر عليك الجهد والوقت في البحث بين أكوام أكثر من 1,196,444 تطبيق!

""",
                      8,
                      4,
                      3,
                      "a"),
                  getCard(
                      "تيك نيوز",
                      "",
                      "assets/deraz.jpg",
                      "لطلبة الذين تم قبولهم مؤخراً من قبل قسم العمارة أو المقبلين على التخصص مستقبلاً كخيار"
                      "ويمكن",
                      14,
                      4,
                      3,
                      "b"),
                ],
              ),
              TableRow(children: [
                getCard(
                    "اراجيك",
                    "",
                    "assets/arageek.png",
                    """مع تزايد الاهتمام بتجربة المستخدم بشكل كبير في السنوات الأخيرة وظهور الكثير من مصممي تجربة المستخدم إلى العالم لا بد لنا أن نسأل إلى أين يتجه الأمر؟ ما الذي يجعل هذا الشخص أو ذاك مؤهلاً لتلك المهنة؟ وهل سيصبح مصمم تجربة المستخدم أهم من المبرمج الذي ينفذ البرامج؟ أم ستتجه تلك المهنة نحو الانهيار؟

""",
                    21,
                    7,
                    13,
                    "c"),
                getCard(
                    "مركز تيكنيكا",
                    "",
                    "assets/nuqta.png",
                    "حدث The new architect هو حدث مهتم با"
                    "لطلبة الذين تم قبولهم مؤخراً من قبل قسم العمارة أو المقبلين على التخصص مستقبلاً كخيار"
                    " أول.يعد الحدث تعريفي، حيث سيتم التعريف بالقسم وكل مايدور حوله من أسئلة تتعلق"
                    " بطرق التدريس، النظم المتبعة، بالإضافة إلى السلبيات والإيجابيات التي تواجه "
                    "طالب العمارة. سيتولى سير الحدث معماريين لهم التجربة والخبرة في المج"
                    "ال إلى جانب عدد من الطلبة القُدامى لأكثر تفاصيل عن الكلية عامة.",
                    33,
                    1,
                    0,
                    "d"),
              ])
            ],
          ),
        ));
  }
}
