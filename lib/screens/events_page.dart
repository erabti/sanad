import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EventsPage extends StatefulWidget {
  @override
  _EventsPageState createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> {
  Widget getCard(
      String name, String label, String image, String postText, int initLike,
      [int comment, int share,String i]) {
    int likes = initLike ?? 3;
    int comments = comment ?? likes - 1;
    int shares = share ?? likes + 2;
    return Card(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                name,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Column(
                children: <Widget>[
                  CircleAvatar(
                    backgroundImage: AssetImage(image),
                    radius: 20,
                  ),
                  Text(label)
                ],
              )
            ],
          ),
          Divider(),
          i == 'a' ? Image.asset("assets/p3.jpeg") : Container(),
          i == 'b' ? Image.asset("assets/p2.jpg") : Container(),

          Text(postText,
              textAlign: TextAlign.start, textDirection: TextDirection.rtl,maxLines: 4,overflow: TextOverflow.ellipsis,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(FontAwesomeIcons.thumbsUp),
                        onPressed: () {
                          setState(() {
                            likes += 1;
                          });
                        },
                      ),
                      Text((likes).toString())
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(FontAwesomeIcons.comments),
                        onPressed: () {
                          setState(() {
                            likes += 1;
                          });
                        },
                      ),
                      Text((comments).toString())
                    ],
                  ),
                ],
              ),
              Column(
                children: <Widget>[
                  IconButton(
                    icon: Icon(FontAwesomeIcons.share),
                    onPressed: () {
                      setState(() {
                        likes += 1;
                      });
                    },
                  ),
                  Text((shares).toString())
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("احداث", style: TextStyle(fontWeight: FontWeight.bold)),
          actions: <Widget>[Icon(Icons.filter_list)],
        ),
        body: SingleChildScrollView(
          child: Table(
            children: <TableRow>[
              TableRow(
                children: <Widget>[
                  getCard(
                      "مركز نقطة",
                      "",
                      "assets/nuqta.png",
                      "حدث The new architect هو حدث مهتم با"
                      "لطلبة الذين تم قبولهم مؤخراً من قبل قسم العمارة أو المقبلين على التخصص مستقبلاً كخيار",
                      8,
                      4,
                      3,
                      "a"),
                  getCard(
                      "دراز كورنر",
                      "",
                      "assets/deraz.jpg",
                      "حدث The new engineer هو حدث مهتم با"
                      "لطلبة الذين تم قبولهم مؤخراً من قبل قسم العمارة أو المقبلين على التخصص مستقبلاً كخيار",
                      14,
                      4,
                      3,
                      "b"),
                ],
              ),
              TableRow(children: [
                getCard(
                    "مركز نقطة",
                    "",
                    "assets/nuqta.png",
                    "حدث The new architect هو حدث مهتم با"
                    "لطلبة الذين تم قبولهم مؤخراً من قبل قسم العمارة أو المقبلين على التخصص مستقبلاً كخيار"
                    " أول.يعد الحدث تعريفي، حيث سيتم التعريف بالقسم وكل مايدور حوله من أسئلة تتعلق"
                    "ال إلى جانب عدد من الطلبة القُدامى لأكثر تفاصيل عن الكلية عامة.",
                    11),
                getCard(
                    "مركز نقطة",
                    "",
                    "assets/nuqta.png",
                    "حدث The new architect هو حدث مهتم با"
                    "لطلبة الذين تم قبولهم مؤخراً من قبل قسم العمارة أو المقبلين على التخصص مستقبلاً كخيار"
                    " أول.يعد الحدث تعريفي، حيث سيتم التعريف بالقسم وكل مايدور حوله من أسئلة تتعلق"
                    " بطرق التدريس، النظم المتبعة، بالإضافة إلى السلبيات والإيجابيات التي تواجه "
                    "طالب العمارة. سيتولى سير الحدث معماريين لهم التجربة والخبرة في المج"
                    "ال إلى جانب عدد من الطلبة القُدامى لأكثر تفاصيل عن الكلية عامة.",
                    11),
              ])
            ],
          ),
        ));
  }
}
