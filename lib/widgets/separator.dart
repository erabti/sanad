import 'package:flutter/material.dart';

class Separator extends StatelessWidget {
  final String text;
  Separator(this.text);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _Line(),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: Text(text),
        ),
        _Line()
      ],
    );
  }
}

class _Line extends StatelessWidget {
  const _Line();
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width * 0.2;
    return Container(color: Colors.black, height: 0.4, width: width);
  }
}
