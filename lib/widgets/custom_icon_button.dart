import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
 
class IconButtons extends StatelessWidget {
  final IconData _icon;
  final MaterialColor _color;
  final VoidCallback _onPressed;

  const IconButtons._(this._icon, this._color, this._onPressed);
  factory IconButtons.facebook(VoidCallback onPressed) => IconButtons._(
        FontAwesomeIcons.facebook,
        Colors.blue,
        onPressed,
      );
  factory IconButtons.google(VoidCallback onPressed) => IconButtons._(
        FontAwesomeIcons.google,
        Colors.red,
        onPressed,
      );
  @override
  Widget build(BuildContext context) {
    return Center(
      child: IconButton(
        padding: EdgeInsets.all(30),
        onPressed: _onPressed,
        icon: Center(
          child: Icon(
            _icon,
            color: _color.shade700,
            size: 50,
          ),
        ),
      ),
    );
  }
}
