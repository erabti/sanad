import 'package:flutter/material.dart';

class Page extends StatelessWidget {
  final bool scrollable;
  final Widget child;

  const Page({Key key, @required this.child, this.scrollable = false})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
          child: !scrollable
              ? child
              : Center(child: SingleChildScrollView(child: child)),
        ),
      ),
    );
  }
}
