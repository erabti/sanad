import 'package:flutter/material.dart';

class Field extends StatelessWidget {
  final String hintText;

  const Field([this.hintText]);
  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.only(bottom: 5),
        child: TextField(
          decoration: InputDecoration(
            hintText: hintText,
            border: UnderlineInputBorder(),
            contentPadding: EdgeInsets.all(15)
          ),
        ),
      );
}
