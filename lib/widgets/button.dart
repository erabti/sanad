import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final VoidCallback onPressed;
  final String label;
  final bool isAccent;

  const Button({
    Key key,
    @required this.onPressed,
    this.label = "",
    this.isAccent = false,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      padding: EdgeInsets.symmetric(horizontal: 80, vertical: 3.5),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      color: isAccent ? Colors.white : null,
      child: Text(
        label,
        style: TextStyle(
          color: isAccent ? Colors.teal : Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 21,
        ),
      ),
    );
  }
}
