import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: 80,
        vertical: MediaQuery.of(context).size.height * 0.05,
      ),
      child: Image.asset("assets/logo.png"),
    );
  }
}
