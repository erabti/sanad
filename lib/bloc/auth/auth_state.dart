part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class LoadingAuthState extends AuthState {}

class AuthenticatedState extends AuthState {}

class UnauthenticatedState extends AuthState {}
