part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent {}

class LoginUser extends AuthEvent {
  final String email;
  final String password;
  LoginUser(this.email, this.password);
}



class ForgotPassword extends AuthEvent {
  final String email;
  ForgotPassword(this.email);
}

class LogoutUser extends AuthEvent {}
