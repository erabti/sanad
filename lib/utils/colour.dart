import 'package:flutter/material.dart';

class Colours {
  Colours._();

  static Gradient get purple => LinearGradient(
      colors: [Colors.purple, Colors.purple.shade700, Colors.purple.shade900]);

  static Gradient get purpleAccent => LinearGradient(colors: [
        Colors.purpleAccent,
        Colors.purpleAccent.shade400,
        Colors.purpleAccent.shade700
      ]);

  static Gradient get green => LinearGradient(colors: [
        Colors.lightGreen.shade300,
        Colors.lightGreen,
        Colors.lightGreen.shade700
      ]);

  static Gradient get greenAccent => LinearGradient(colors: [
        Colors.lightGreenAccent.shade200,
        Colors.lightGreenAccent,
        Colors.lightGreenAccent.shade700
      ]);

  static Gradient get red => LinearGradient(
      colors: [Colors.red.shade400, Colors.red, Colors.red.shade700]);

  static Gradient get redAccent => LinearGradient(
      colors: [Colors.red.shade300, Colors.red.shade400, Colors.red.shade500]);

  static Gradient get grey => LinearGradient(
      colors: [Colors.grey, Colors.grey.shade600, Colors.grey.shade700]);

  static Gradient get yellow => LinearGradient(colors: [
        Colors.yellow.shade700,
        Colors.yellow.shade800,
        Colors.yellow.shade900
      ]);

  static Gradient get yellowAccent => LinearGradient(colors: [
        Colors.yellowAccent.shade200,
        Colors.yellowAccent.shade400,
        Colors.yellowAccent.shade700
      ]);
  static Gradient get blue => LinearGradient(colors: [
    Colors.lightBlueAccent,
    Colors.lightBlue,
    Colors.blue.shade700
  ]);
  static Gradient get lime => LinearGradient(
    colors: [
      Colors.lime.shade500,
      Colors.lime.shade700,
      Colors.lime.shade900
    ]
  );
}
